package com.devcamp.splitstringapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SplitStringAPI {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true, name = "string") String requestString) {
        ArrayList<String> splitStringArrayList = new ArrayList<>();
        String[] stringArray = requestString.split(" ");
        for (String stringElement : stringArray) {
            splitStringArrayList.add(stringElement);

        }
        return splitStringArrayList;
    }

}
